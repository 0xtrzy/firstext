import inkex
from inkex import Rectangle

import random

class MakeRectangle(inkex.EffectExtension):
    def effect(self):

        my_rectangle = Rectangle()

        rect_width = random.randrange(10, 50)
        rect_height = random.randrange(10, 50)

        rect_x = random.randrange(0, 200)
        rect_y = random.randrange(0, 200)

        my_rectangle.set('x', rect_x)
        my_rectangle.set('y', rect_y)

        my_rectangle.set('width', rect_width)
        my_rectangle.set('height', rect_height)

        my_rectangle.style['fill'] = f'rgb({random.randrange(0, 255)}, {random.randrange(0, 255)}, {random.randrange(0, 255)})'
        my_rectangle.style['stroke'] = f'rgb({random.randrange(0, 255)}, {random.randrange(0, 255)}, {random.randrange(0, 255)})'

        my_rectangle.style['stroke-width'] = f'{random.randrange(1, 10)}'

        current_layer = self.svg.get_current_layer()

        current_layer.append(my_rectangle)


if __name__ == '__main__':
    MakeRectangle().run()
